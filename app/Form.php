<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Form extends Model
{
    protected $table = 'tbl_forms';
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id',
        'col_user_id',
        'col_first_name',
        'col_middle_name',
        'col_last_name',
        'col_email',
        'col_mobile_number',
        'col_phone_number',
        'col_country',
        'col_address',
        'col_subject',
        'col_message'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'col_user_id');
    }

}
