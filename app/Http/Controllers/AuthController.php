<?php

namespace App\Http\Controllers;

use App\Exceptions\UnauthorizedException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $validate_user = Auth::validate(['email' => $email, 'password' => $password]) ;
        if (!$validate_user) {
            throw new UnauthorizedException('Email or password are incorrect');
        }
        $token = Auth::attempt(['email' => $email, 'password' => $password]) ;
        $user = Auth::setToken($token)->user();
        $user['token'] = $token;
        return response()->json($user, 200);
    }

    public function authUser()
    {
        if(Auth::check()){
            $user = Auth::user() ;
            $user['token'] = auth()->login($user);
            return response()->json($user, 200);
        }else{
            throw new UnauthorizedException('UnAuthorized');
        }
    }

    public function logout()
    {
        Auth::logout();
    }

}
