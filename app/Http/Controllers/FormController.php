<?php

namespace App\Http\Controllers;

use App\Exceptions\NotFoundException;
use App\Exceptions\ValidationException;
use App\Http\Requests\Form\StoreRequest;
use App\Http\Resources\Form\FormResource;
use Illuminate\Http\Request;
use App\Form;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();
        $forms = FormResource::collection(DB::table('tbl_forms')->where('col_user_id', $user_id)->get());
        return response()->json($forms, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $user_id = Auth::id();
        $formCreated = Form::create([
            'col_user_id' => $user_id,
            'col_first_name' => $request['first_name'],
            'col_middle_name' => $request['middle_name'],
            'col_last_name' => $request['last_name'],
            'col_email' => $request['email'],
            'col_mobile_number' => $request['mobile_number'],
            'col_phone_number' => $request['phone_number'],
            'col_country' => $request['country'],
            'col_address' => $request['address'],
            'col_subject' => $request['subject'],
            'col_message' => $request['message'],
        ]);
        $formCreatedResource = new FormResource($formCreated);
        return response()->json($formCreatedResource, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $form = Form::find($id);
        if (!$form) {
            throw new NotFoundException('Form Not Found');
        }
        $formResource = new FormResource($form);
        return response()->json($formResource, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = Auth::id();
        $form = Form::find($id);
        if (!$form) {
            throw new NotFoundException('Form Not Found');
        }
        Form::where('id', $id)->where('col_user_id', $user_id)
            ->update([
                'col_first_name' => $request['first_name'],
                'col_middle_name' => $request['middle_name'],
                'col_last_name' => $request['last_name'],
                'col_email' => $request['email'],
                'col_mobile_number' => $request['mobile_number'],
                'col_phone_number' => $request['phone_number'],
                'col_country' => $request['country'],
                'col_address' => $request['address'],
                'col_subject' => $request['subject'],
                'col_message' => $request['message'],
            ]);
        $formResource = new FormResource(Form::find($id));
        return response()->json($formResource, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = Form::find($id);
        if (!$form) {
            throw new NotFoundException('Form Not Found');
        }
        $form->delete();
        return response()->noContent();
    }

    public function deletedForms()
    {
        $user_id = Auth::id();
        $deletedForms = FormResource::collection(DB::table('tbl_forms')->where('col_user_id', $user_id)->where('deleted_at','!=' ,null)->get());
        return response()->json($deletedForms, 200);
    }

}
