<?php

namespace App\Http\Resources\Form;

use Illuminate\Http\Resources\Json\JsonResource;

class FormResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->col_user_id,
            'first_name' => $this->col_first_name,
            'middle_name' => $this->col_middle_name,
            'last_name' => $this->col_last_name,
            'email' => $this->col_email,
            'mobile_number' => $this->col_mobile_number,
            'phone_number' => $this->col_phone_number,
            'country' => $this->col_country,
            'address' => $this->col_address,
            'subject' => $this->col_subject,
            'message' => $this->col_message,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ];
    }
}
