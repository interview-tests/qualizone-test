<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('col_user_id');
            $table->text('col_first_name');
            $table->string('col_middle_name')->nullable();
            $table->string('col_last_name');
            $table->string('col_email')->unique();
            $table->string('col_mobile_number');
            $table->string('col_phone_number')->nullable();
            $table->string('col_country');
            $table->string('col_address')->nullable();
            $table->string('col_subject');
            $table->text('col_message')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_forms');
    }
}
